# AI Pong


Just a quick test for myself from [Catcoder](https://catcoder.codingcontest.org/).

### Goal:
Create an Pong-AI better that the one in simulator.jar .

### Status:
My AI sometimes beats it, fundamentally it calculates where the ball should hit and moves the paddle there.
