package ai;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

/**
 * @author Kacper Urbaniec
 * @version 04.01.2019
 */
public class ai {

    // Calculates the postion of the ball, to determine where the
    // paddle needs to be moved
    public static double[] calculate(double[] ball) {
        // ball <x> <y> <vx> <vy>
        int left = 25;
        int top = 0;
        while(ball[0] > 25) {
            ball[0] += ball[2];
            ball[1] += ball[3];
            if(ball[1] < 20 || ball[1] > 580) {
            //if(ball[1] < 30 || ball[1] > 570) {
                ball[3] = -ball[3];
            }
        }
        System.out.println("Destination: x " + ball[0] + " y "+ ball[1]);
        return ball;
    }


    public static void main(String[] args) {
        ArrayList<int[]> player = new ArrayList<>();
        ArrayList<int[]> cpu = new ArrayList<>();
        ArrayList<double[]> ball = new ArrayList<>();
        Socket s;
        BufferedReader in;
        PrintWriter out;
        try {
            s = new Socket("localhost", 7000);
            in = new BufferedReader(new InputStreamReader(s.getInputStream()));
            out = new PrintWriter(s.getOutputStream());
            String str;
            boolean begin = true;
            double d = 0;
            String td = "";
            for(;;) {
                str = in.readLine();
                // update
                if(str.equals("update")) {
                    //System.out.println("move 36");
                    //System.out.println(ball.get(ball.size()-1)[0]);
                    double[] before = null;
                    if (ball.size() > 2) {
                        before = ball.get(ball.size() - 2);
                    }
                    double[] lastb = ball.get(ball.size() - 1);
                    int[] lastp = player.get(player.size() - 1);

                    if (lastb[2] > 0) {
                        //System.out.println("Ball away!");
                        if (lastp[0] + 75 > 300) {
                            int dis = (lastp[0] + 75) - 300;
                            dis /= 6;
                            //System.out.println("Runter: " + (lastp[0]+75));
                            out.println("move -" + dis + "");
                        } else if (lastp[0] + 75 < 300) {
                            int dis = 300 - lastp[0] - 75;
                            dis /= 6;
                            //System.out.println("Hoch: " + (lastp[0]+75));
                            out.println("move " + dis + "");
                        } else {
                            out.println("move 0");
                        }

                    } else {
                        //System.out.println("Ball coming back!");
                        if (before != null) {
                            //System.out.println("here");
                            if (before[2] > 0 && lastb[2] < 0) {
                                begin = false;
                                System.out.println("Hitting from opponent");
                                double[] pos = ai.calculate(lastb);
                                if (lastp[0] + 75 < pos[1] + 7.5) {
                                    d = (pos[1] + 7.5) - (lastp[0] + 75);
                                    System.out.println("Need to move " + d);
                                    td = "";
                                    //System.out.println("D " + d);
                                    //out.println("move "+d+"");

                                } else {
                                    d = (lastp[0] + 75) - (pos[1] + 7.5);
                                    System.out.println("Need to move " + d);
                                    td = "-";
                                    //System.out.println("D " + d);
                                    //out.println("move -"+d+"");
                                }
                            }
                            if (before[2] < 0 && lastb[2] > 0) {
                                out.println("move 0");
                            }
                        }
                        if (d != 0) {
                            System.out.println("D");
                            if (d - 36 > 0) {
                                d -= 36;
                                out.println("move " + td + "36");
                            } else {
                                out.println("move " + td + d + "");
                                d = 0;
                            }
                        } else {
                            out.println("move 0");
                        }
                        if (before == null || lastb[0] < 10 || begin) {
                            begin = true;
                            System.out.println("Hey");
                            d = 0;
                            if (lastp[0] + 75 < lastb[1] + 7.5) {
                                double dis = (lastb[1] + 7.5) - (lastp[0] + 75);
                                //d /= 5;
                                out.println("move " + dis + "");
                                //out.println("move 36");
                            } else {
                                double dis = (lastp[0] + 75) - (lastb[1] + 7.5);
                                //d /= 5;
                                out.println("move -" + dis + "");
                                //out.println("move -36");
                            }
                        }
                        //System.out.println("here2");
                        /**
                         if (flag) {
                         //System.out.println("Just drifting");
                         if (lastp[0] + 75 < lastb[1]+7.5) {
                         double dis = (lastb[1]+7.5)-(lastp[0] + 75);
                         //d /= 5;
                         out.println("move "+dis+"");
                         //out.println("move 36");
                         } else {
                         double dis = (lastp[0] + 75)-(lastb[1]+7.5);
                         //d /= 5;
                         out.println("move -"+dis+"");
                         //out.println("move -36");
                         }
                         }
                         flag = true;*/
                    }
                    out.flush();
                }
                // player - me
                else if (str.contains("player")) {
                   String[] split = str.split("\\s+");
                   int[] conv = new int[2];
                   conv[0] = Integer.parseInt(split[1]);
                   conv[1] = Integer.parseInt(split[2]);
                   player.add(conv);
                }
                // cpu - opponent
                else if(str.contains("cpu")) {
                    String[] split = str.split("\\s+");
                    int[] conv = new int[2];
                    conv[0] = Integer.parseInt(split[1]);
                    conv[1] = Integer.parseInt(split[2]);
                    cpu.add(conv);
                }
                // ball
                else {
                    String[] split = str.split("\\s+");
                    double[] conv = new double[4];
                    conv[0] = Double.parseDouble(split[1]);
                    conv[1] = Double.parseDouble(split[2]);
                    conv[2] = Double.parseDouble(split[3]);
                    conv[3] = Double.parseDouble(split[4]);
                    ball.add(conv);
                }
            }

        } catch (Exception ex) {
            System.out.println("test" + ex.getMessage());
        }
    }
}
